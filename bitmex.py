import requests 
import time
import sys
from utils import generate_signature, dict_to_proper_body

URL = "https://www.bitmex.com"

ENDPOINTS = {
    "chat_connected":"/api/v1/chat/connected",
    "chat":"/api/v1/chat",
}

apiKey = None
apiSecret = None

for sy in sys.argv:
    sy_spl = sy.split("=")
    if sy_spl[0] == "--api-key":
        apiKey = sy_spl[1]
    elif sy_spl[0] == "--api-secret":
        apiSecret = sy_spl[1]

expires = int(round(time.time()) + 5)
data = {"message":"Hi everyone"}
signature = generate_signature(
    apiSecret, 
    'POST', 
    ENDPOINTS["chat"], 
    expires, 
    dict_to_proper_body(data))

headers={
    "Content-Type": "application/json",
    "api-expires":str(expires),
    "api-key":apiKey,
    "api-signature":signature
    }

r = requests.post(
    url = "{}{}".format(URL,ENDPOINTS["chat"]),
    headers = headers,
    data=dict_to_proper_body(data)
    ) 
  
data = r.text
print(r.headers)
print(data)
    
